   
help () {
    valid_options=(
    back
    blank
    commands
    finish_project
    list_projects
    options
    rm_pg
    select_project
    start_project
    versions
    )
    
    case $1 in 
    '')
    print PDF Factory
    print "This little sofware helps you to modify PDF files, keep history of your work,"
    print "and go back to a previous state"
    print "For a list of available options of this help,"
    print "Type 'help options'"
    ;;

    'back')
    print "Go back to a previous version."
    print "The version should be specified by a integer that can be found by invoking 'versions' command"
    print "If no version is given, go back to the last version before current"
    ;;
    
    'blank')
    print "Insert blank page after page specified"
    print "Enter 0 to add a blank page before the first one"
    ;;
    
    'commands')
    print "Some commands (see below) are hooked."
    print "If you want to use original command, please type 'command' before invoking it"
    print "Note that you must not specify input nor output file, as it is handled by the program"
    print "Available commands are:"
    print -l $hooked_commands
    ;;

    'finish_project')
    print "Finish current project."
    print "Every file is removed. History and settings are kept."
    print "Project is moved to old projects and can be found by entering 'list_projects old'"
    ;;

    
    'list_projects')
    print "Print current projects. To find older projects, enter 'list_projects old'"
    ;;
    
    'options')
    print -l $valid_options
    ;;

    'rm_pg')
    print 'Remove pages entered'
    print 'You can enter one page or more with spaces between as following:'
    print 'rm_pg 1 50 450'
    print 'You can also enter a range of pages:'
    print 'rm_pg 40-50'
    ;;
    
    'select_project')
    print "Select a project. You must give a number matching with numbers given by 'list_projects' output."
    print "If no number is given, the last one is selected"
    ;;

    'start_project')
    print "Select a PDF file to transform and pass it to this command"
    ;;

    'versions')
    print "Print the previous versions of the project."
    print "Number at the start of the line is the version number required by 'back' command"
    ;;
    
    *)
    print "Unknown options"
    help options
    ;;
    esac
    # Printing newline for clarity
    print
}
