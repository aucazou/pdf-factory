# Hooked commands
# TODO create a SUCCESS parameter?

hooked_commands=(
briss
gs
krop
mainlulu.sh
okular
pdfcrop
pdfjam
pdftk
pdftocairo
xtndncntr.sh
)

BRISS_PATH="/save/scripts/Pdf_factory/briss-0.10/briss-0.10.jar"
PATH+=":/save/scripts/lulu"

_this=${(%):-%N}
_folder=${_this%/*}

## Base hook function
function _as_pdftk () {
    # The output file must be specied after 'output' parameter
    cmd=$1
    shift
    command $cmd $INPUT_FILE $@ output $OUTPUT_FILE
    return $?
}

function _as_pdftocairo () {
    # The output must be specified at the end of the parameters
    cmd=$1
    shift
    command $cmd $INPUT_FILE $@ $OUTPUT_FILE
    return $?
}

function _as_pdfjam () {
    # -o
    cmd=$1
    shift
    command $cmd $INPUT_FILE $@ -o $OUTPUT_FILE
    return $?
}

function _as_ghostscript () {
    # -sOutputFile
    cmd=$1
    shift
    command $cmd -dSAFER -dBATCH -dNOPAUSE $@ -sOutputFile=$OUTPUT_FILE -sDEVICE=pdfwrite $INPUT_FILE 
    return $?
}


declare -A _cmd_n_name
_cmd_n_name[gs]=_as_ghostscript
_cmd_n_name[krop]=_as_pdfjam
_cmd_n_name[mainlulu.sh]=_as_pdftocairo
_cmd_n_name[pdfcrop]=_as_pdftocairo
_cmd_n_name[pdfjam]=_as_pdfjam
_cmd_n_name[pdftk]=_as_pdftk
_cmd_n_name[pdftocairo]=_as_pdftocairo
_cmd_n_name[xtndncntr.sh]=_as_pdftocairo

function _end_base_hook_function () {
    _mv_files
    if [[ $? -eq 0 ]]; then 
        ((SETTING_VALUES[version]++))
        _save_settings
    fi
}

function _history () {
    success=$?
    cmd=$1
    shift
    if [[ $success -ne 0 ]]; then
        print "[FAILED] $cmd $@" >> $PROJECT_HISTORY
    else
        print "$SETTING_VALUES[version] $cmd $@ " >> $PROJECT_HISTORY
    fi
}


for name cmd in ${(kv)_cmd_n_name}; do
    f1="function ${name} () { "
    f2='eval $_check_project_selected;'
    f3="$cmd $name"
    f3b=' $@;'
    f3=$f3$f3b
    f4="_history $name"
    f4b=' $@;'
    f4=$f4$f4b
    f5="_end_base_hook_function;}"
    total=$f1$f2$f3$f4$f5
    eval $total
    # This eval is the only way to avoid problems with scope
done

function blank () {
    printerr No history is saved. Please fix.
    return -1
    if [[ -z $1 ]] ; then
        printerr "Please enter a page number"
        return -1
    fi
    pnb=$1
    blank_file=$_folder/blank.pdf
    if [[ $pnb == 0 ]]; then 
        command pdftk $blank_file $INPUT_FILE cat output $OUTPUT_FILE
    else
        ((pnb_plus_one = pnb + 1))
        command pdftk A=$INPUT_FILE B=$blank_file cat A1-$1 B1 A$pnb_plus_one-end output $OUTPUT_FILE
    fi
    _history blank $@
    _end_base_hook_function
}

function briss () {
    eval $_check_project_selected
    java -jar $BRISS_PATH $INPUT_FILE
    _history briss
    _end_base_hook_function
}

rm_pg () {
    # accepts page (one or more) or ranges: 5-12
    nb=$(qpdf --show-npages $INPUT_FILE)
    py_cmd="import mkrange; print(mkrange.make_range('$@', $nb))"
    pg_range=$(python -c $py_cmd)
    command pdftk $INPUT_FILE cat $pg_range output $OUTPUT_FILE
    return $?
}


## commands that don't modify state
_non_modifying_cmds=(
    okular
)
for cmd in $_non_modifying_cmds ; do
    l1="function $cmd () {"
    l2="command $cmd "
    l2b='$INPUT_FILE $@;'
    l2=$l2$l2b
    l3='}'
    total=$l1$l2$l3
    eval $total
done
    

# TODO ajouter fonctions de lulu
