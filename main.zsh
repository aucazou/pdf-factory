# source this file
# TODO
# problems to come:
# a project selected in more than one place
# multiple files
# only call help for a specific command... This is not handled: pdftk -h

# parameters
export PDF_FACTORY=~/.pdf_factory
export CURRENT_PROJECTS=$PDF_FACTORY/currents_projects
export OLD_PROJECTS=$PDF_FACTORY/old_projects

export PROJECT_HISTORY=".history"
export PROJECT_SETTINGS=".settings"
export OUT_SUFFIX="_out"
declare -A SETTING_VALUES
_list_of_project_parameters=(SETTING_VALUES INPUT_FILE OUTPUT_FILE PROJECT_SELECTED PROJECT_DIR)

# DEBUG FUNCTIONS
_debug () {
    print INPUT_FILE $INPUT_FILE
    print OUTPUT_FILE $OUTPUT_FILE
    print SETTING_VALUES $SETTING_VALUES
    if (( ${+SETTING_VALUES} )) ; then
        print settings value: oui
    fi
}
_empty_parameters () {
    INPUT_FILE=""
    OUTPUT_FILE=""
    SETTING_VALUES=()
}

_this=${(%):-%N}
_folder=${_this%/*}
source $_folder/help.zsh
source $_folder/hooked.zsh


# This command should be called before each function used
# by the user that mandates a project has been selected
# Call it this way: eval $_check_project_selected
_check_project_selected='if [[ ${+INPUT_FILE} -eq 0 ]]; then printerr "Please select a project."; return -1; fi'

# util commands
printerr () {
    print "$@" 1>&2
}

# functions
_append_project () {
    nb=`cat $CURRENT_PROJECTS | wc -l`
    ((nb++))
    print "$nb $1" >> $CURRENT_PROJECTS
    }

_load_settings () {
    if (( $ARGC )) ; then
        SETTING_VALUES=($@)
        # I can't remember why the 'eval' is important so
        # I keep it just in case
        #eval "SETTING_VALUES=($@)"
    else
        out=`cat $PROJECT_SETTINGS`
        eval "SETTING_VALUES=($out)"
    fi
}

_mv_files () {
    if [[ -a $OUTPUT_FILE ]]; then 
        # We save the previous version of the file
        ((v=$SETTING_VALUES[version]-1))
        mv $INPUT_FILE .${INPUT_FILE%.pdf}.$v.pdf
        mv $OUTPUT_FILE $INPUT_FILE
        return 0
    fi
    return -1
}

_save_settings () {
    print ${(kv)SETTING_VALUES} > $PROJECT_SETTINGS
}

_select_project () {
    pushd $1
    PROJECT_DIR=$1
    _load_settings
    INPUT_FILE=$SETTING_VALUES[file]
    OUTPUT_FILE=${INPUT_FILE%.pdf}${OUT_SUFFIX}.pdf
    PROJECT_SELECTED=$1
}

# BUG
# soit ce n'est pas clair, soit le programme ne revient pas à la version rpécédente. à voir
# en outre, si la commande est back, ça ne marche pas pour la copie des fichiers car il n'y a pas de fichier correspondant
back () {
    eval $_check_project_selected
    goal=$1
    if [[ $ARGC -eq 0 ]]; then
        # we need to go back to the version before the last, since current version is not yet saved
        ((goal=$SETTING_VALUES[version]-2))
    fi
    if [[ $goal -lt 0 ]]; then
        printerr "First version is zero"
        return -1
    fi

    if [[ $goal -ge $SETTING_VALUES[version] ]]; then
        printerr "Last version is $SETTING_VALUES[version]"
        return -1
    fi

    # No error!
    print "$SETTING_VALUES[version] back $goal" >> $PROJECT_HISTORY
    cp ".${INPUT_FILE%.pdf}.$goal.pdf" $OUTPUT_FILE
    _mv_files

    ((SETTING_VALUES[version]++))
    _save_settings
    print "Back to version $goal"
}

finish_project () {
    eval $_check_project_selected
    if read -q "choice?Do you really want to finish the project? Press [Y/y] "; then
        pushd $PROJECT_DIR
        rm .*.pdf 
        rm *.pdf
        print "\nProject finished"
        print "Project finished" >> $PROJECT_HISTORY
        # put in old projects file
        projects=`cat $CURRENT_PROJECTS`
        projects=(${(ps:\n:)projects})
        rm $CURRENT_PROJECTS
        touch $CURRENT_PROJECTS
        i=1
        for p in $projects; do
            line=(${(s. .)p})
            if [[ $line[2] != $PROJECT_SELECTED ]]; then
                print "$i $line[2]" >> $CURRENT_PROJECTS
            fi
        done
        print $PROJECT_SELECTED >> $OLD_PROJECTS
        # unset variables
        for var in $_list_of_project_parameters; do
            eval $var'=""'
        done
        popd
    fi

}

list_projects () {
    if [[ $1 == "old" ]]; then
        p=$OLD_PROJECTS
    else
        p=$CURRENT_PROJECTS
    fi
    cat $p
}
    
preexec () {
}

precmd () {
    print Please note that you are writing commands in the special PDF factory
    if [[  (( ${+PROJECT_SELECTED})) && ${PROJECT_SELECTED} != '' ]]; then
        print "Project selected: ${PROJECT_SELECTED}"
    fi
    print Type 'help' for more info
}

select_project () {
    last_line=`tail -n 1 $CURRENT_PROJECTS`
    if [[ $last_line == '' ]]; then 
        printerr "No project was created. Please start a project"
        return -1
    fi
    last_line=(${(s. .)last_line})
    last_nb=$last_line[1]

    if (( $ARGC )) ; then
        nb=$1
        if [[ $nb -gt $last_nb ]]; then
            printerr "There is no project matching with $nb"
            return -1
        fi
    else
        nb=$last_nb
    fi

    projects=`cat $CURRENT_PROJECTS`
    projects=(${(ps:\n:)projects})
    for p in $projects; do
        split=(${(s. .)p})
        if [[ $nb -eq $split[1] ]]; then
            _select_project $split[2]
            print "Project selected: ${PROJECT_SELECTED}"
            break
        fi
    done

        

}

start_project () {
# TODO check that project does not yet exist
# TODO check that $1 is only a file, and not a path
# TODO add a title
    _rmv_non_alnum () { 
        x=\"$1\"
        python -c "print(''.join([c if c.isalnum() or c in '.-' else '_' for c in $x]))"
    }
    nname=`_rmv_non_alnum $1`
    project=$PDF_FACTORY/${nname}_project
    mkdir $project
    folder=$PWD
    cp $1 $project/$nname
    pushd $project
    print "0 Started $project" > $PROJECT_HISTORY

    declare -A tmp_settings
    tmp_settings[file]=$nname
    tmp_settings[folder]=\"$folder\"
    tmp_settings[version]=1
    _load_settings ${(kv)tmp_settings}
    _save_settings

    _append_project $project
    popd
    _select_project $project
    
}

versions () {
    eval $_check_project_selected
    # the last extra space after '*' is important
    cat $PROJECT_HISTORY | grep '^[0-9]* '
}

# Create folders and files if necessary
if [[ ! -d $PDF_FACTORY ]]; then
    mkdir $PDF_FACTORY
    touch $CURRENT_PROJECTS
    touch $OLD_PROJECTS
fi
    


