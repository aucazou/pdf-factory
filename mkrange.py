def produce_ints(str):
   """Generator that takes a string of the type
   '1 4 6 7 10-34 49-55'
   and yields numbers matching with these integers or ranges
   """

   for elt in str.split(" "):
       if not elt:
          continue
       try:
           yield int(elt)
       except ValueError:
           try:
               start, end = [int(x) for x in elt.split("-")]
           except (IndexError, ValueError):
               raise ValueError(f"'{elt}' is not a valid value")
           if end < start:
               raise ValueError(f"Error in range: {elt}")
           for i in range(start, end + 1):
               yield i

def make_range(pages, end):
    """Creates a range for pdftk command
    The pages are supposed to be removed, but pdftk
    admits only pages to include.
    end is the last page of the file."""

    pages = list(produce_ints(pages))
    if too_high := [pg for pg in pages if pg > end ]:
        raise ValueError(f"Please enter number inferior to end: {too_high}")
    full_pages = [pg for pg in range(1, end+1) if pg not in pages]
    ranges = f"{full_pages[0]}"
    last_pg = first_member = full_pages[0]
    for pg in full_pages[1:]:
        if pg - last_pg > 1:
            if last_pg == first_member:
                ranges += f" {pg}"
            else:
                ranges += f"-{last_pg} {pg}"
            first_member = pg
        last_pg = pg
    ranges += f"-{full_pages[-1]}"
    return ranges
